# Output if such sha1 not exists in Wikimedia Commons:
# {"batchcomplete":"","query":{"allimages":[]}}
# Useful to check - if such image already uploaded to Commons.

hash=`sha1sum "$1" | head -c 40`

echo sha1:
echo $hash

curl --silent "https://commons.wikimedia.org/w/api.php?action=query&list=allimages&format=json&aisha1=$hash" | jq
echo

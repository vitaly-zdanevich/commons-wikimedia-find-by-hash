I want to upload a file to Wikimedia Commons and asking myself: is this file already uploaded by somebody else, with another name and to a different category? To check - I use https://yandex.ru/images with `site:commons.wikimedia.org`, and now I have this script that checks by sha1.

Use:

```
commons-wikimedia-find-by-hash.sh myfile.jpg
```

![video](/video.webm)

Available in Gentoo Guru https://github.com/gentoo/guru/tree/master/net-misc/commons-wikimedia-find-by-hash

WebExtension about the same https://gitlab.com/vitaly-zdanevich/webextension-wikimedia-commons-search-by-hash

See related documentation https://commons.wikimedia.org/wiki/Commons:Tools#Duplicates_and_Hash-based_search
